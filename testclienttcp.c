/* tcpclient.c */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <signal.h>


#define SERVER_PORT_INJECT 514
#define SERVER_PORT_VM 520
#define BUFFER_SIZE 2048

//
// void sigquit() {
//     send(sock, message, strlen(message), 0);
//     close(sock);
//
//     exit(0);
// }
//

int sock;

void funcatexit(void) {

  send(sock, "q", 1, 0);
  close(sock);

}


int main()

{

        int bytes;
        char message[BUFFER_SIZE];
        char timestamp[16];


        int sockRecu;
        struct sockaddr_in nameRecu;
        //struct hostent *hp, *gethostbyname();

        //sockRecu = init_connection(SERVER_PORT_INJECT, nameRecu, 1, tmp1);
        sockRecu = socket(AF_INET, SOCK_DGRAM, 0);
        if (sockRecu < 0)   {
          perror("Opening sockaddr_in datagram socket");
          exit(1);
        }
        nameRecu.sin_family = AF_INET;
        nameRecu.sin_addr.s_addr = htonl(INADDR_ANY);
        nameRecu.sin_port = htons(SERVER_PORT_INJECT);
        if (bind(sockRecu, (struct sockaddr *) &nameRecu, sizeof(nameRecu))) {
          perror("binding datagram socket");
          exit(1);
        }


        //
        //
        // int bytes_recieved;
        // char send_data[BUFFER_SIZE],recv_data[BUFFER_SIZE];
        // struct hostent *host;
        // struct sockaddr_in server_addr;
        //
        // host = gethostbyname("51.255.62.77");
        //
        // if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        //     perror("Socket");
        //     exit(1);
        // }
        //
        // server_addr.sin_family = AF_INET;
        // server_addr.sin_port = htons(SERVER_PORT_VM);
        // server_addr.sin_addr = *((struct in_addr *)host->h_addr);
        // bzero(&(server_addr.sin_zero),8);


        while(1) {


                  int bytes_recieved;
                  char send_data[BUFFER_SIZE],recv_data[BUFFER_SIZE];
                  struct hostent *host;
                  struct sockaddr_in server_addr;

                  host = gethostbyname("51.255.62.77");

                  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
                      perror("Socket");
                      exit(1);
                  }

                  server_addr.sin_family = AF_INET;
                  server_addr.sin_port = htons(SERVER_PORT_VM);
                  server_addr.sin_addr = *((struct in_addr *)host->h_addr);
                  bzero(&(server_addr.sin_zero),8);



          timestamp[0] = 's';

          printf("Waiting for messages\n\n");

          // if ((bytes = read(sockRecu, message, BUFFER_SIZE)) > 0) {
          //
          //   message[bytes] = '\0';
          //   FILE *f = popen("date +\"\%b \%e \%H:\%M:\%S\"", "r");
          //   fgets(timestamp, sizeof(timestamp), f);
          //   pclose(f);
          //
          //
          // } else {
          //   printf("message error");
          // }



          //
          // //connextion serveur
          // if (connect(sock, (struct sockaddr *)&server_addr,
          //             sizeof(struct sockaddr)) == -1)
          // {
          //     perror("Connect error");
          //     exit(1);
          // }


          //creation fork
          int pid ;

          /*
            FORK à cet emplacement
           */
          if((pid = fork()) == 0){

            atexit(funcatexit);


          //connextion serveur
          if (connect(sock, (struct sockaddr *)&server_addr,
                      sizeof(struct sockaddr)) == -1)
          {
              perror("Connect error");
              exit(1);
          }


          printf("Ready to send\n");

          while(1) {
            char test[16] = "test\n";
            send(sock, test, strlen(test), 0);


          }


        }
       else { //process parent, sleep 4min30 avant kill du fils




          sleep(10);
          printf("Fin de message\n");

          kill(pid, SIGKILL);

                    send(sock,"q",strlen("q"), 0);
                    close(sock);
                    sock = -1;

        }
      }
return 0;
}
