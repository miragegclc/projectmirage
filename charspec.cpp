/// Looks for and replaces all, returns new length 
int ReplaceStringKeys(char * forString, const int length)
{
	int newLength = length;
	std::cout<<"\nreplace length : "<<length;
	char buf[5];
	memset(buf, '\0', 4); //ptr, val, num
	buf[0] = '#'; // First #
	buf[1] = '0';

	for (int i = 0; i < length; ++i)
	{
		if (i < length -1 && ((unsigned char)forString[i]) < 32 && forString[i] >= 0)
		{
			int val = forString[i];
			/// dst, src, length
			memmove(forString + i + 3, forString + i, length);
			sprintf(buf+2, "%o", val); // target, "format", data
			int intLen = strlen(buf);
			memmove(forString + i, buf, intLen);
			newLength += 3;
		}
		/// Convert our magic character 141 to \n to separate the log messages approriately. 
		if (((unsigned char)forString[i]) == 141)
		{
			forString[i] = '\n';
		}
	}
	std::cout<<"\nFinal str: "<<forString;
	return newLength;
}